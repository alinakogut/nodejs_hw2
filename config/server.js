module.exports = {
    port: process.env.PORT || 8080,
    dbPort: 27017,
    dbHost: 'localhost',
    dbName: 'node_hw2',
    dbUrl: 'mongodb+srv://smuser:smuserpassword@node.hkv4s.mongodb.net/nodejs_hw2?retryWrites=true&w=majority'
}