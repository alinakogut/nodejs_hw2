const jwt = require('jsonwebtoken');
const User = require('../models/user');

const {
    secret
} = require('../config/auth');

module.exports.register = (req, res) => {
    const {
        username,
        password
    } = req.body;

    if (!username || !password) {
        return res.status(400).json({
            message: 'Enter username and password'
        })
    }

    const user = new User({
        username,
        password,
        createdDate: new Date()
    });
    user.save()
        .then(() => {
            res.json({
                message: 'Success'
            });
        })
        .catch(err => {
            res.status(500).json({
                message: err.message
            });
        });
};

module.exports.login = (req, res) => {
    const {
        username,
        password
    } = req.body;

    if (!username || !password) {
        return res.status(400).json({
            message: 'Enter username and password'
        });
    }

    User.findOne({
            username,
            password
        }).exec()
        .then(user => {
            if (!user) {
                return res.status(400).json({
                    message: 'No user found'
                });
            }
            res.json({
                message: 'Success',
                token: jwt.sign(JSON.stringify(user), secret)
            });
        })
};