const Note = require('../models/note');

module.exports.getNotes = (req, res) => {
    const userId = req.user._id;

    Note.find({
            userId
        }).exec()
        .then((notes) => {
            if (!notes) {
                return res.status(400).json({
                    message: 'There is no any notes'
                });
            }

            res.json({
                notes
            });
        })
        .catch(err => {
            res.status(500).json({
                message: err.message
            });
        });
};

module.exports.getNoteById = (req, res) => {
    const noteId = req.params.id;

    if (!noteId) {
        return res.status(400).json({
            message: 'Incorrect data'
        });
    }

    Note.findById(noteId).exec()
        .then((note) => {
            if (!note) {
                return res.status(400).json({
                    message: 'There is no such note'
                });
            }

            res.json({
                note
            });
        })
        .catch(err => {
            res.status(500).json({
                message: err.message
            });
        });
};

module.exports.addNote = (req, res) => {
    const userId = req.user._id;
    const {
        text,
        completed
    } = req.body;

    if (!text || !completed) {
        return res.status(400).json({
            message: 'Incorrect data'
        });
    }

    const note = new Note({
        userId,
        text,
        completed,
        createdDate: new Date()
    });

    note.save()
        .then(() => {
            res.json({
                message: 'Success'
            });
        })
        .catch(err => {
            res.status(500).json({
                message: err.message
            });
        });
};

module.exports.deleteNote = (req, res) => {
    const noteId = req.params.id,
        userId = req.user._id;

    if (!noteId) {
        return res.status(400).json({
            message: 'Incorrect data'
        });
    }

    Note.findOneAndDelete({
            _id: noteId,
            userId
        }).exec()
        .then(() => {
            res.json({
                message: 'Success'
            });
        })
        .catch(err => {
            res.status(500).json({
                message: err.message
            });
        });
};

module.exports.updateNote = (req, res) => {
    const noteId = req.params.id,
        userId = req.user._id;

    if (!noteId) {
        return res.status(400).json({
            message: 'Incorrect data'
        });
    }

    Note.findOneAndUpdate({
            _id: noteId,
            userId
        }, {
            $set: req.body
        }).exec()
        .then(() => {
            res.json({
                message: 'Success'
            });
        })
        .catch(err => {
            res.status(500).json({
                message: err.message
            });
        });
};


module.exports.changeNotesStatus = (req, res) => {
    const noteId = req.params.id,
        userId = req.user._id;

    if (!noteId) {
        return res.status(400).json({
            message: 'Incorrect data'
        });
    }

    let notesStatus;

    Note.findById(noteId).exec()
        .then((note) => {
            if (!note) {
                return res.status(400).json({
                    message: 'There is no such note'
                });
            }
            
            notesStatus = note.completed
        })
        .then(() => Note.findOneAndUpdate({
            _id: noteId,
            userId
        }, {
            $set: {
                completed: !notesStatus
            }
        }).exec())
        .then(() => {
            res.json({
                message: 'Success'
            });
        })
        .catch(err => {
            res.status(500).json({
                message: err.message
            });
        });
};