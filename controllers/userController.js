const User = require('../models/user');


module.exports.getProfileInfo = (req, res) => {
    const userId = req.user._id;

    User.findById(userId).exec()
        .then((user) => {
            if (!user) {
                return res.status(400).json({
                    message: 'There is no such user'
                });
            }

            res.json({
                user: {
                    _id: user._id,
                    username: user.username,
                    createdDate: user.createdDate
                }
            });
        })
        .catch(err => {
            res.status(500).json({
                message: err.message
            });
        });
}

module.exports.deleteProfile = (req, res) => {
    const userId = req.user._id;

    if (!userId) {
        return res.status(400).json({
            message: 'There is no such user'
        });
    }

    User.findOneAndDelete({
            userId
        }).exec()
        .then(() => {
            res.json({
                message: 'Success'
            });
        })
        .catch(err => {
            res.status(500).json({
                message: err.message
            });
        });
}

module.exports.changePassword = (req, res) => {
    const userId = req.user._id;

    if (!userId) {
        return res.status(400).json({
            message: 'There is no such user'
        });
    }

    User.updateOne({
            _id: userId
        }, {
            password: req.body.newPassword
        }).exec()
        .then((user) => {
            if (!user) {
                return res.status(400).json({
                    message: 'There is no such user'
                });
            }

            if (req.body.password !== user.password) {
                res.status(400).json({
                    message: "Incorrect password"
                });
            }
            res.json({
                message: 'Success'
            });
        })
        .catch(err => {
            res.status(500).json({
                message: err.message
            });
        });
}