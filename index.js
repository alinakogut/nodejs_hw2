const express = require('express');
const mongoose = require('mongoose');
const app = express();

const {
    port,
    dbUrl
} = require('./config/server');

const userRouter = require('./routers/userRouter');
const authRouter = require('./routers/authRouter');
const noteRouter = require('./routers/noteRouter');

mongoose.connect(dbUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
});

app.use(express.json());

app.use('/api', userRouter);
app.use('/api', authRouter);
app.use('/api', noteRouter);

app.listen(port, () => console.log(`Server is running on port ${port}...`));