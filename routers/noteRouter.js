const express = require('express');
const router = express.Router();

const {
    addNote,
    deleteNote,
    getNotes,
    updateNote,
    getNoteById,
    changeNotesStatus
} = require('../controllers/noteController');
const authorize = require('../middlewares/authMiddleware');

router.get('/notes', authorize, getNotes);
router.get('/notes/:id', authorize, getNoteById);
router.post('/notes', authorize, addNote);
router.delete('/notes/:id', authorize, deleteNote);
router.put('/notes/:id', authorize, updateNote);
router.patch('/notes/:id', authorize, changeNotesStatus);

module.exports = router;