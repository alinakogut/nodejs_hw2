const express = require('express');
const router = express.Router();

const {
    getProfileInfo,
    deleteProfile,
    changePassword
} = require('../controllers/userController');
const authorize = require('../middlewares/authMiddleware');

router.get('/users/me', authorize, getProfileInfo);
router.delete('/users/me', authorize, deleteProfile);
router.patch('/users/me', authorize, changePassword);

module.exports = router;